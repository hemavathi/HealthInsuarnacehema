import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TestHealth {


	static double calculateInsuranceprimium(String name,String gen,int age, Map<String, String> currenthealthMap, Map<String, String> habbitsthMap) {
		double basePremium=5000;
		int baseAge=25;
		if(age>18 && age<=25)
			basePremium=basePremium+getpercentage(basePremium, 10);
		else 
		{
			int perc=((age-baseAge)/5)*20;
			System.out.println("perc "+perc);
			basePremium=basePremium+getpercentage(basePremium, perc);

		}


		if(currenthealthMap!=null ) {
			if(currenthealthMap.get("BloodPressure").toString().equalsIgnoreCase("yes"))
				basePremium=basePremium+getpercentage(basePremium, 1);
			if( currenthealthMap.get("BloodSugar").toString().equalsIgnoreCase("yes"))
				basePremium=basePremium+getpercentage(basePremium, 1);
			if( currenthealthMap.get("Overweight").toString().equalsIgnoreCase("yes"))
				basePremium=basePremium+getpercentage(basePremium, 1);
			if(currenthealthMap.get("Hypertension").toString().equalsIgnoreCase("yes"))
				basePremium=basePremium+getpercentage(basePremium, 1);
		}
		if(habbitsthMap!=null) {

			if(habbitsthMap.get("dailyExercise").equalsIgnoreCase("yes"))
				basePremium=basePremium-getpercentage(basePremium, 3);

			if(habbitsthMap.get("drugs").equalsIgnoreCase("yes"))
				basePremium=basePremium+getpercentage(basePremium, 3);
			if(habbitsthMap.get("alcohol").equalsIgnoreCase("yes"))
				basePremium=basePremium-getpercentage(basePremium, 3);
			if(habbitsthMap.get("smoking").equalsIgnoreCase("yes"))
				basePremium=basePremium+getpercentage(basePremium, 3);
		}



		return basePremium;

	}

	static double getpercentage(double amnt,int percentage) {


		return (amnt/100)*percentage;



	}
	public static void main(String[] args) {


		System.out.println("Enter name :");
		Scanner scanner=new Scanner(System.in);
		String name=scanner.nextLine();
		System.out.println("name is "+name);
		System.out.println("Enter Gender : ");
		scanner=new Scanner(System.in);
		String gen=scanner.nextLine();
		System.out.println("Enter Age : ");
		scanner=new Scanner(System.in);
		int age=scanner.nextInt();

		System.out.println("please provide current health details with ans yes or No");

		System.out.println("Hypertension : ");
		scanner=new Scanner(System.in);
		String hyperTension=scanner.nextLine();

		System.out.println("Blood pressure : ");
		scanner=new Scanner(System.in);
		String bloodPressure=scanner.nextLine();

		System.out.println("Blood sugar : ");
		scanner=new Scanner(System.in);
		String bloodSugar=scanner.nextLine();

		System.out.println("Overweight : ");
		scanner=new Scanner(System.in);
		String overweight=scanner.nextLine();

		System.out.println("please provide details about your Habits with ans yes or No");

		System.out.println("Smoking : ");
		scanner=new Scanner(System.in);
		String smoking=scanner.nextLine();

		System.out.println("Alcohol : ");
		scanner=new Scanner(System.in);
		String alcohol=scanner.nextLine();

		System.out.println("Daily exercise : ");
		scanner=new Scanner(System.in);
		String dailyExercise=scanner.nextLine();

		System.out.println("Drugs : ");
		scanner=new Scanner(System.in);
		String drugs=scanner.nextLine();

		Map<String, String> currenthealthMap=new HashMap<>();
		currenthealthMap.put("Hypertension", hyperTension);
		currenthealthMap.put("BloodPressure", bloodPressure);
		currenthealthMap.put("BloodSugar", bloodSugar);
		currenthealthMap.put("Overweight", overweight);


		Map<String, String> habbitsthMap=new HashMap<>();
		habbitsthMap.put("smoking", smoking);
		habbitsthMap.put("alcohol", alcohol);
		habbitsthMap.put("dailyExercise", dailyExercise);
		habbitsthMap.put("drugs", drugs);
		double insuranceprimiem=calculateInsuranceprimium(name, gen,age,currenthealthMap,habbitsthMap);

		if(name.contains(" "))
			name=name.split(" ")[0];
		if(gen.toLowerCase().startsWith("f"))
			name="Miss."+name;
		if(gen.toLowerCase().startsWith("m"))
			name="Mr."+name;

		System.out.println("Health Insurance Premium for"+name +": Rs."+insuranceprimiem);



	}

}
